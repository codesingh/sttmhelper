# README #

SikhiToTheMAX - Helper is a set of programs developed to allow users to control SikhiToTheMAX software remotely.

There is a windows application that runs on the computer that SikhiToTheMAX is installed on and there is a mobile application that runs on a mobile device.

### Current Features ###
* This is ALPHA software at the moment
* Allow users to pause and restart SikhiToTheMAX Akhand Paat Mode from a mobile phone 
* Allow users to increase and decrease the speed of Akhand Paat Mode from a mobile phone 
* Connect multiple devices to a server.
* Password is required to connect to server.

### Requirements ###
* SikhiToTheMAX 2 is required. This system will not work with SikhiToTheMAX 1
* Windows application needs to be running SikhToTheMAX in Akhand Paat Mode before it can be controlled.
* Computer and mobile need to be on the same network, in the same IP address range.
* The mobile app will loose connection if it becomes inactive or if the mobile screen turns off


### How do I get set up? ###
#### Windows Application ####
* Download exe file from download section
* Run the exe, as Administrator, on the windows computer.
* When firewall setting appear tick all networks and click allow.
* The program will show the password screen. 
* Enter a password and then click on the box below the password box.

#### Mobile Application ####
* Download apk file from download section. 
* Run the app
* Go to settings tab, enter the required password and click connect.
* The status text at the bottom of the app will change when a connection is made.
* Go to Akhand Paat display tab.
* Click on the buttons to check that the mobile app can connect and control SikhiToTheMAX.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin