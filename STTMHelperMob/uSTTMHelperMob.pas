unit uSTTMHelperMob;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.TabControl,
  FMX.StdCtrls, IPPeerClient, IPPeerServer, System.Tether.Manager,
  System.Tether.AppProfile, FMX.Layouts, FMX.Edit, System.Actions, FMX.ActnList,
  FMX.Platform, FMX.StdActns;

type
  TApplicationEvent = (aeFinishedLaunching, aeBecameActive, aeWillBecomeInactive,
    aeEnteredBackground, aeWillBecomeForeground, aeWillTerminate, aeLowMemory,
    aeTimeChange, aeOpenURL);

  TApplicationEventHandler = function(AAppEvent: TApplicationEvent; AContext: TObject): Boolean of object;

  IFMXApplicationEventService = interface(IInterface)
  ['{F3AAF11A-1678-4CC6-A5BF-721A24A676FD}']
    procedure SetApplicationEventHandler(AEventHandler: TApplicationEventHandler);
   end;

type
  TFrmSTTMHelperMob = class(TForm)
    PanelHeader: TPanel;
    LblHeader: TLabel;
    TcSTTM: TTabControl;
    TiAkhandPaatDisplay: TTabItem;
    BtnSlower: TSpeedButton;
    BtnFaster: TSpeedButton;
    BtnPauseResume: TSpeedButton;
    TetheringManagerSTTMMob: TTetheringManager;
    TetheringAppProfileSTTMMob: TTetheringAppProfile;
    PanelFooter: TPanel;
    LblStatus: TLabel;
    GridPanelLayout1: TGridPanelLayout;
    TmrSTTMHelperMob: TTimer;
    TiSettings: TTabItem;
    TbPassword: TEdit;
    BtnConnect: TCornerButton;
    TiSlideView: TTabItem;
    procedure BtnConnectClick(Sender: TObject);
    procedure TetheringManagerSTTMMobRequestManagerPassword(
      const Sender: TObject; const RemoteIdentifier: string;
      var Password: string);
    procedure BtnPauseResumeClick(Sender: TObject);
    procedure TetheringManagerSTTMMobPairedToRemote(const Sender: TObject;
      const AManagerInfo: TTetheringManagerInfo);
    procedure BtnSlowerClick(Sender: TObject);
    procedure BtnFasterClick(Sender: TObject);
    procedure TmrSTTMHelperMobTimer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    function HandleAppEvent(AAppEvent: TApplicationEvent; AContext: TObject): Boolean;
    procedure FormCreate(Sender: TObject);
  private
    FIsConnected : Boolean;
    procedure CheckServer;
    procedure DisconnectFromServer;

    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmSTTMHelperMob: TFrmSTTMHelperMob;

implementation

{$R *.fmx}

procedure TFrmSTTMHelperMob.BtnConnectClick(Sender: TObject);
var
  I: Integer;
begin
  if TetheringManagerSTTMMob.PairedManagers.Count = 0 then
  begin
    TetheringManagerSTTMMob.Enabled := True;
    TetheringManagerSTTMMob.AutoConnect();
  end
  else
  begin
    for I := TetheringManagerSTTMMob.PairedManagers.Count - 1 downto 0 do
      TetheringManagerSTTMMob.UnPairManager(TetheringManagerSTTMMob.PairedManagers[I]);
  end;
  CheckServer;
end;

procedure TFrmSTTMHelperMob.BtnFasterClick(Sender: TObject);
begin
  TetheringAppProfileSTTMMob.RunRemoteAction(TetheringManagerSTTMMob.RemoteProfiles[0]
                                            , 'ActionFaster');
end;

procedure TFrmSTTMHelperMob.BtnPauseResumeClick(Sender: TObject);
begin
  TetheringAppProfileSTTMMob.RunRemoteAction(TetheringManagerSTTMMob.RemoteProfiles[0]
                                            , 'ActionPauseResume');
end;

procedure TFrmSTTMHelperMob.BtnSlowerClick(Sender: TObject);
begin
  TetheringAppProfileSTTMMob.RunRemoteAction(TetheringManagerSTTMMob.RemoteProfiles[0]
                                            , 'ActionSlower');
end;

procedure TFrmSTTMHelperMob.TetheringManagerSTTMMobPairedToRemote(
  const Sender: TObject; const AManagerInfo: TTetheringManagerInfo);
begin
  BtnConnect.Text := 'Disconnect';
  CheckServer;
end;

procedure TFrmSTTMHelperMob.TetheringManagerSTTMMobRequestManagerPassword(
  const Sender: TObject; const RemoteIdentifier: string; var Password: string);
begin
  Password := TbPassword.Text;
end;

procedure TFrmSTTMHelperMob.TmrSTTMHelperMobTimer(Sender: TObject);
begin
  CheckServer;
end;

procedure TFrmSTTMHelperMob.CheckServer;
var
  I: Integer;
  ConnectedManager : String;
begin
  if TetheringManagerSTTMMob.PairedManagers.Count > 0 then
  begin
    for I := 0 to TetheringManagerSTTMMob.PairedManagers.Count - 1 do
    begin
      ConnectedManager := ConnectedManager
                            + ' - '
                            + TetheringManagerSTTMMob.PairedManagers[I].ManagerText;
    end;
    LblStatus.Text := 'Working with :' + ConnectedManager;
    FIsConnected := true;
    BtnConnect.Text := 'Disconnect';
  end
  else
  begin
    LblStatus.Text := 'Not connected';
    FIsConnected := false;
    BtnConnect.Text := 'Connect';
  end;
end;

procedure TFrmSTTMHelperMob.DisconnectFromServer;
var
  I: Integer;
begin
  if TetheringManagerSTTMMob.PairedManagers.Count > 0 then
  begin
    for I := TetheringManagerSTTMMob.PairedManagers.Count - 1 downto 0 do
      TetheringManagerSTTMMob.UnPairManager(TetheringManagerSTTMMob.PairedManagers[I]);
  end;
end;

procedure TFrmSTTMHelperMob.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  TmrSTTMHelperMob.Enabled := False;
  DisconnectFromServer;
end;

procedure TFrmSTTMHelperMob.FormCreate(Sender: TObject);
var aFMXApplicationEventService: IFMXApplicationEventService;
begin
  if TPlatformServices.Current.SupportsPlatformService(IFMXApplicationEventService, IInterface(aFMXApplicationEventService)) then
  begin
    aFMXApplicationEventService.SetApplicationEventHandler(HandleAppEvent);
  end;
end;

procedure TFrmSTTMHelperMob.FormShow(Sender: TObject);
begin
  TmrSTTMHelperMob.Enabled := True;
end;

function TFrmSTTMHelperMob.HandleAppEvent(AAppEvent: TApplicationEvent; AContext: TObject): Boolean;
begin
  case AAppEvent of
    //aeFinishedLaunching: Log('Finished Launching');
    //aeBecameActive: Log('Became Active');
    //aeWillBecomeInactive: Log('Will Become Inactive');
    aeEnteredBackground: DisconnectFromServer;
    //aeWillBecomeForeground: Log('Will Become Foreground');
    //aeWillTerminate: Log('Will Terminate');
    //aeLowMemory: Log('Low Memory');
    //aeTimeChange: Log('Time Change');
    //aeOpenURL: Log('Open URL');
  end;
  Result := True;
end;

end.
