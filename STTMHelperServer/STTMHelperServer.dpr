program STTMHelperServer;

uses
  Vcl.Forms,
  uSTTMHelperServer in 'uSTTMHelperServer.pas' {FrmSTTMHelper},
  Vcl.Themes,
  Vcl.Styles;

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'Sikhi To The Max Helper Server';
  TStyleManager.TrySetStyle('Silver');
  Application.CreateForm(TFrmSTTMHelper, FrmSTTMHelper);
  Application.Run;
end.
