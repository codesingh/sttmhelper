object FrmSTTMHelper: TFrmSTTMHelper
  Left = 0
  Top = 0
  Caption = 'Sikhi To The Max Helper'
  ClientHeight = 178
  ClientWidth = 198
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Visible = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PcSTTMHelper: TPageControl
    Left = 0
    Top = 0
    Width = 198
    Height = 178
    ActivePage = TsSlides
    Align = alClient
    TabOrder = 0
    object TsLog: TTabSheet
      Caption = 'Log'
      ExplicitWidth = 156
      object LbxConnectedDevices: TListBox
        Left = 0
        Top = 21
        Width = 190
        Height = 129
        Align = alClient
        ItemHeight = 13
        TabOrder = 0
        ExplicitWidth = 156
      end
      object TbPassword: TEdit
        Left = 0
        Top = 0
        Width = 190
        Height = 21
        Align = alTop
        TabOrder = 1
        TextHint = 'Enter password'
        OnExit = TbPasswordExit
        ExplicitWidth = 156
      end
    end
    object TsAkhandPath: TTabSheet
      Caption = 'Akhand Path'
      ImageIndex = 1
      object GridPanel1: TGridPanel
        Left = 0
        Top = 0
        Width = 190
        Height = 150
        Align = alClient
        ColumnCollection = <
          item
            Value = 50.000000000000000000
          end
          item
            Value = 50.000000000000000000
          end>
        ControlCollection = <
          item
            Column = 1
            Control = BtnFaster
            Row = 0
          end
          item
            Column = 0
            ColumnSpan = 2
            Control = BtnSpace
            Row = 1
          end
          item
            Column = 0
            Control = BtnSlower
            Row = 0
          end>
        RowCollection = <
          item
            Value = 50.000000000000000000
          end
          item
            Value = 50.000000000000000000
          end
          item
            SizeStyle = ssAuto
          end>
        TabOrder = 0
        object BtnFaster: TSpeedButton
          Left = 95
          Top = 1
          Width = 94
          Height = 74
          Action = ActionFaster
          Align = alClient
          ExplicitLeft = 48
          ExplicitTop = 88
          ExplicitWidth = 23
          ExplicitHeight = 22
        end
        object BtnSpace: TSpeedButton
          Left = 1
          Top = 75
          Width = 188
          Height = 74
          Action = ActionPauseResume
          Align = alClient
          ExplicitLeft = 48
          ExplicitTop = 88
          ExplicitWidth = 23
          ExplicitHeight = 22
        end
        object BtnSlower: TSpeedButton
          Left = 1
          Top = 1
          Width = 94
          Height = 74
          Action = ActionSlower
          Align = alClient
          ExplicitLeft = 28
          ExplicitTop = 19
          ExplicitWidth = 23
          ExplicitHeight = 22
        end
      end
    end
    object TsSlides: TTabSheet
      Caption = 'Slides'
      ImageIndex = 2
      object LbShabad: TListBox
        Left = 0
        Top = 33
        Width = 190
        Height = 84
        Align = alClient
        ItemHeight = 13
        TabOrder = 0
        ExplicitTop = 25
        ExplicitHeight = 96
      end
      object BtnPrevSlide: TButton
        Left = 0
        Top = 0
        Width = 190
        Height = 33
        Align = alTop
        Caption = 'Load Slides'
        TabOrder = 1
        OnClick = BtnPrevSlideClick
      end
      object BtnNextSlide: TButton
        Left = 0
        Top = 117
        Width = 190
        Height = 33
        Align = alBottom
        Caption = 'Load Slides'
        TabOrder = 2
        OnClick = BtnNextSlideClick
        ExplicitTop = 8
      end
    end
  end
  object AmSTTMHelper: TActionManager
    Left = 248
    Top = 168
    StyleName = 'Platform Default'
    object ActionFaster: TAction
      Caption = 'Faster ( + )'
      OnExecute = ActionFasterExecute
    end
    object ActionSlower: TAction
      Caption = 'Slower ( - )'
      OnExecute = ActionSlowerExecute
    end
    object ActionPauseResume: TAction
      Caption = 'Pause / Resume (SPACE)'
      OnExecute = ActionPauseResumeExecute
    end
  end
  object TetheringManagerSTTMServer: TTetheringManager
    OnUnPairManager = TetheringManagerSTTMServerUnPairManager
    OnRemoteManagerShutdown = TetheringManagerSTTMServerRemoteManagerShutdown
    Password = 'nanak1469'
    Text = 'STTM Server'
    Left = 72
    Top = 40
  end
  object TetheringAppProfileSTTMServer: TTetheringAppProfile
    Manager = TetheringManagerSTTMServer
    Text = 'STTMServer'
    Group = 'STTM'
    Actions = <
      item
        Name = 'ActionPauseResume'
        IsPublic = True
        Action = ActionPauseResume
        NotifyUpdates = False
      end
      item
        Name = 'ActionFaster'
        IsPublic = True
        Action = ActionFaster
        NotifyUpdates = False
      end
      item
        Name = 'ActionSlower'
        IsPublic = True
        Action = ActionSlower
        NotifyUpdates = False
      end>
    Resources = <>
    Left = 68
    Top = 96
  end
  object TmrSTTMServer: TTimer
    Interval = 15000
    OnTimer = TmrSTTMServerTimer
    Left = 116
    Top = 128
  end
end
